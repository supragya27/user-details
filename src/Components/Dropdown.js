import React from 'react'
import './style/Dropdown.css'

function Dropdown(
    {list,
    setCurrent,
    purpose,
    setSugg=()=>{}}) {
    return (
        list.length!==0 ?
        <div className="DDown" >
            {purpose==="Travellers" ? <span className="labelT"> Saved Travellers:</span> : null}
            {
            list.map((item,idx)=>
            <div key={idx} onMouseDown={()=>{if(purpose==="Travellers") setCurrent(idx); else {setCurrent(item); setSugg([]) }} }>
                {item}
            </div> )
            }            
        </div> :
        null
    )
}

export default Dropdown
