import React, { useState,useEffect } from 'react'
import {nationIdList, genderList, mobileCodeList} from '../Constants/constants'
import './style/UserDetails.css'
import Dropdown from './Dropdown'
import Select from './Select'
import Input from './Input'
import Button from './Button'

function UserDetails() {
    const [suggNations, setSuggNations] = useState([])
    const [showTraveller, setShowTraveller] = useState(false)
    const [showNations, setShowNations] = useState(false)
    const [travellerData, setTravellerData] = useState([])
    const [travellerNames, setTravellerNames] = useState([])
    const [id, setId] = useState(0)
    const [initial, setInitial] = useState(true)
    //user details:
    const [firstName, setfirstName] = useState('')
    const [lastName, setlastName] = useState('')
    const [gender, setGender] = useState('Female')
    const [mobileNo, setmobileNo] = useState(0)
    const [mobCode, setmobCode] = useState("+91")
    const [email, setemail] = useState()
    const [nation, setNation] = useState('')
    const [date, setdate] = useState("2000-01-01")

    useEffect(()=>{
        fetch('http://localhost:8000/traveller')
  .then(response => response.json())
  .then(data => {
    setTravellerData(data)
    setfirstName(data[0].name.split(' ')[0])
    setlastName(data[0].name.split(' ')[1])
    setGender(data[0].gender)
    setmobCode(data[0].phone.split(' ')[0])
    setmobileNo(data[0].phone.split(' ')[1])
    setemail(data[0].email)
    setNation(data[0].nationality)
    setdate(data[0].DOB)
  })
    },[])
    useEffect(()=>{
        if(initial)
        {
            setInitial(false)
        }
        else
        {
            setfirstName(travellerData[id].name.split(' ')[0])
            setlastName(travellerData[id].name.split(' ')[1])
            setGender(travellerData[id].gender)
            setmobCode(travellerData[id].phone.split(' ')[0])
            setmobileNo(travellerData[id].phone.split(' ')[1])
            setemail(travellerData[id].email)
            setNation(travellerData[id].nationality)
            setdate(travellerData[id].DOB)
            setSuggNations([])
        }},[id])
        useEffect(()=>{
            let array=travellerData.map(t=>t.name)
            setTravellerNames(array)
        },[travellerData])
        const onNationChange=(e)=>{
            const val=e.target.value;
            setNation(val)
            if(val.length===0)
            {
                let arr=nationIdList.map(n=>n.en)
                setSuggNations(arr)
            }
            else
            {
                let arr=[]
                if((val[0]>='A'&&val[0]<='Z')||(val[0]>='a'&&val[0]<='z'))
                arr=nationIdList.map(n=>n.en)
                else
                arr=nationIdList.map(n=>n.ar)

                setSuggNations( arr.sort().filter(v=>v.includes(val)) )
            }
        }
        const onSubmit=()=>{
            console.log("Name: "+firstName+" "+lastName);
            console.log("Gender: "+gender);
            console.log("Mobile Number: "+mobileNo);
            console.log("Nationality: "+nation);
            console.log("Date of Birth: "+date);
        }
        return (
            <div>
                    <div data-testid="heading" className="heading">User Details</div>

                    <div data-testid="subheadingTNG" className="labelData"> Traveller Name and Gender</div>
                    {showTraveller? <Dropdown list={travellerNames} setCurrent={setId} purpose="Travellers"/>:null}
                    <Input value={firstName} onFocus={()=>{setShowTraveller(true)}} onBlur={()=>{setShowTraveller(false)}} onChange={(e)=>{setfirstName(e.target.value)}}/>
                    <Input value={lastName} onFocus={()=>{setShowTraveller(true)}} onBlur={()=>{setShowTraveller(false)}} onChange={(e)=>{setlastName(e.target.value)}}/>
                    <Select list={genderList} value={gender} onChange={(e)=>setGender(e.target.value)}/>
                    
                    <div data-testid="subheadingMob" className="labelData"> Mobile Number </div>
                    <Select list={mobileCodeList} value={mobCode}  onChange={(e)=>setmobCode(e.target.value)}/>
                    <Input value={mobileNo} onChange={e=>setmobileNo(e.target.value)}/>
                    
                    <div data-testid="subheadingEmail" className="labelData"> Email Address </div>           
                    <Input value={email} onChange={e=>setemail(e.target.value)}/>
                    
                    <div data-testid="subheadingNDOB" className="labelData"> Nationality and Date of Birth </div>                
                    { showNations ? <Dropdown setCurrent={setNation} list={suggNations} setSugg={setSuggNations} purpose="Nations"/> :null } 
                    <Input  onFocus={()=>setShowNations(true)} onBlur={()=>setShowNations(false)} value={nation} onChange={onNationChange}/>
                    <Input classCSS="inputD" type="date" value={date} onChange={e=>setdate(e.target.value)}/>
                    <hr/>
                    <Button text="Save" onClick={onSubmit} />
            </div>
        )
}

export default UserDetails