import React from 'react'
import './style/Input.css'

function Input({
    type="text",
    classCSS="inputF",
    value,
    onChange,
    onFocus=()=>{},
    onBlur=()=>{}
}) {
    return (
        <input type={type} value={value} className={classCSS} onChange={onChange} data-testid="inp" onFocus={onFocus} onBlur={onBlur} />
    )
}

export default Input
