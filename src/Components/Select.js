import React from 'react'

function Select({
    list,
    value,
    onChange}) {
    return (
        <select value={value} onChange={onChange}>
            {
                list.map((item,idx)=>{
                     return <option key={idx} value={item.value}>{item.value}</option>
                })
            }
        </select>
    )
}

export default Select
