export const nationIdList = [
    { countryCode: 'SA', en: 'Saudi Arabia', ar: 'المملكة العربية السعودية' },
    { countryCode: 'AE', en: 'United Arab Emirates', ar: 'الإمارات العربية المتحدة' },
    { countryCode: 'BH', en: 'Bahrain', ar: 'البحرين' },
    { countryCode: 'KW', en: 'Kuwait', ar: 'الكويت' },
    { countryCode: 'QA', en: 'Qatar', ar: 'دولة قطر' },
    { countryCode: 'OM', en: 'Oman', ar: 'سلطنة عمان' }
]

export const genderList = [
    {value:"Male"},
    {value:"Female"},
    {value:"Other"}
]

export const mobileCodeList = [
    {value:"+966"},
    {value:"+971"},
    {value:"+973"},
    {value:"+965"},
    {value:"+974"},
    {value:"+968"},
    {value:"+91"}
]