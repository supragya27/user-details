import React from "react";
import ReactDOM from "react-dom"
import { render } from "@testing-library/react";
import UserDetails from "../Components/UserDetails";

it('UserDetails renders without crashing', () => {
    const div=document.createElement("div")
    ReactDOM.render(<UserDetails/>,div)
   });

describe('component rendering',()=>{
    test('renders Heading',()=>{
        const {getByTestId}=render(<UserDetails/>)
         expect(getByTestId('heading')).toBeTruthy()
    })

    test('renders sub heading TravellerName and Gender',()=>{
        const {getByTestId}=render(<UserDetails/>)
         expect(getByTestId('subheadingTNG')).toBeTruthy()
    })

    test('renders sub heading Mobile Number',()=>{
        const {getByTestId}=render(<UserDetails/>)
         expect(getByTestId('subheadingMob')).toBeTruthy()
    })

    test('renders sub heading Email',()=>{
        const {getByTestId}=render(<UserDetails/>)
         expect(getByTestId('subheadingEmail')).toBeTruthy()
    })
    
    test('renders sub heading Nationality and DOB',()=>{
        const {getByTestId}=render(<UserDetails/>)
         expect(getByTestId('subheadingNDOB')).toBeTruthy()
    })
})