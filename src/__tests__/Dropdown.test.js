import React from "react";
import ReactDOM from "react-dom"
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Dropdown from "../Components/Dropdown";

const dummyList=['one','two','three']
const dummyPurpose="test"

describe("Dropdown component tests", () => {
    it('Dropdown renders without crashing', () => {
        const div=document.createElement("div")
        ReactDOM.render(<Dropdown list={dummyList} purpose={dummyPurpose} setCurrent={()=>{}} setSugg={()=>{}} />,div)
       });

  test('Current option gets selected', () => {
    const mockCurrent = jest.fn();
    const mockSugg = jest.fn();
    const {getByText} = render(<Dropdown list={dummyList} purpose={dummyPurpose} setCurrent={mockCurrent} setSugg={mockSugg} />);
    userEvent.click(getByText(dummyList[0]));
    expect(mockCurrent).toBeCalled();
  });

  test('Suggestions get cleared', () => {
    const mockCurrent = jest.fn();
    const mockSugg = jest.fn();
    const {getByText} = render(<Dropdown list={dummyList} purpose={dummyPurpose} setCurrent={mockCurrent} setSugg={mockSugg} />);
    userEvent.click(getByText(dummyList[0]));
    expect(mockSugg).toBeCalled();
  });
});
