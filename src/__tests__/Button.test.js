import React from "react";
import ReactDOM from "react-dom"
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Button from "../Components/Button";

describe("Button component tests", () => {
    it('Button renders without crashing', () => {
        const div=document.createElement("div")
        ReactDOM.render(<Button/>,div)
       });

  test('onClick works correctly and text gets loaded', () => {
    const mockClick = jest.fn();
    const {getByText} = render(<Button text="Save" onClick={mockClick}/>);
    userEvent.click(getByText("Save"));
    expect(mockClick).toBeCalled();
  });
});
