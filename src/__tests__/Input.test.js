import React from "react";
import ReactDOM from "react-dom"
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Input from "../Components/Input";

describe("Input component tests", () => {
    it('Input renders without crashing', () => {
        const div=document.createElement("div")
        const value="test"
        const func=()=>{}
        ReactDOM.render(<Input value={value} onChange={func} />,div)
       });

  test('Input responds on click to display the dropdown', () => {
    const mockChange = jest.fn();
    const val="test"
    const {getByTestId} = render(<Input value={val} onChange={mockChange} onFocus={mockChange} />);
    userEvent.click(getByTestId("inp"));
    expect(mockChange).toBeCalled();
  });
});
